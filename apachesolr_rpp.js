(function($) {
  Drupal.behaviors.apachesolrRpp = {
    attach: function (context, settings) {
      $('.apachesolr-rpp-select-list').change(function(e) {
        window.location = Drupal.settings.apachesolrRpp.searchUrls[$(this).val()];
      });
    }
  };
}(jQuery));
